# whatsapp


## Introduction

WhatsApp (also called WhatsApp Messenger) is an internationally available freeware, cross-platform, centralized instant messaging (IM) and voice-over-IP (VoIP) service owned by American company Meta Platforms.  It allows users to send text and voice messages,  make voice and video calls, and share images, documents, user locations, and other content. WhatsApp's client application runs on mobile devices, and can be accessed from computers.[15] The service requires a cellular mobile telephone number to sign up. In January 2018, WhatsApp released a standalone business app called WhatsApp Business which can communicate with the standard WhatsApp client. 


## Getting started on your web browser

https://web.whatsapp.com/


## Link Device 


![](medias/80d22234125-1683074711.png) 




## Getting started on your LINUX Os

Install WhatsApp for Linux
on Ubuntu

https://snapcraft.io/install/whatsapp-for-linux/ubuntu

![](medias/1672643708-screenshot.png)

## Getting it on Ubuntu 

![](medias/screenshot-1672997398.png)





